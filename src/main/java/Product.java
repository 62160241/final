
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ckitt
 */
public class Product implements Serializable {
    private String name;
    private String brand;
    private double price;
    private int amount;
    private int number;
    
    Product(String name,String brand, double price, int amount){
        this.name=name;
        this.brand=brand;
        this.price=price;
        this.amount=amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    void setNumber(int index){
        number= index;
    }
    int getNumber(){
        return number;
    }

    @Override
    public String toString() {
        return "("+getNumber()+")Name:"+getName()+" ["+getBrand()
                +"] Price:"+getPrice()+" Amount:"+getAmount();
    }
    
}
