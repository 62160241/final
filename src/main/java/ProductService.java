
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ckitt
 */
public class ProductService {
    private static ArrayList<Product> productList = new ArrayList<>();
    
    static void addTestData(){
        productList.add(new Product("Lays","LA",10,2));
        productList.add(new Product("Cola","coke",30,4));
    }
    
    static{
        load();
    }
    
    // add
    public static boolean addProduct(Product product){
        productList.add(product);
        save();
        return true;
    }
    // del
    public static boolean delProduct(Product product){
        productList.remove(product);
        save();
        return true;
    }
    public static boolean delProduct(int index){
        productList.remove(index);
        save();
        return true;
    }
    public static boolean delAllProduct(){
        productList.removeAll(productList);
        save();
        return true;
    }
    
    // get
    public static ArrayList<Product> getProduct(){
        return productList;
    }
    public static Product getProduct(int index){
        return productList.get(index);
    }
    
    // update
    public static boolean updateProduct(int index, Product product){
        productList.set(index, product);
        save();
        return true;
    }
    
    public static void save(){
        File file= null;
        FileOutputStream fos = null;
        ObjectOutputStream oos=null;
        try{
            file = new File("joi.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        }catch(FileNotFoundException ex){
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load(){
        File file= null;
        FileInputStream fis = null;
        ObjectInputStream ois=null;
        try{
            file = new File("joi.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        }catch(FileNotFoundException ex){
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<Product> addList(){
        int i=1;
        for(Product e:productList){
            e.setNumber(i);
            i++;
        }
        return productList;
    }
}
